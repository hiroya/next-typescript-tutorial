import * as React from 'react';
import Link from "next/link";
import fetch from "isomorphic-unfetch";
import { NextFunctionComponent } from "next";
import Layout from "../components/MyLayout";

type Props = { shows?: any[] | undefined | null }

const Index: NextFunctionComponent<Props> = (props) => (
  <Layout>
    <h1>Batma TV Shows</h1>
    <ul>
      {props.shows.map(show => (
        <li key={show.id}>
          <Link as={`/p/${show.id}`} href={`/post?id=${show.id}`}>
            <a>{show.name}</a>
          </Link>
        </li>
      ))}
    </ul>
  </Layout>
);

Index.getInitialProps = async () => {
  const res = await fetch("https://api.tvmaze.com/search/shows?q=batman");
  const data = await res.json();
  // server side only
  console.log(`Show data fetched. Count: ${data.length}`);

  return {
    shows: data.map(entry => entry.show)
  }
}

export default Index;
