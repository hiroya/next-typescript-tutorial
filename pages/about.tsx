import * as React from "react";
import Layout from "../components/MyLayout";

const About: React.FunctionComponent = ()  => (
  <Layout>
    <p>this is the about page</p>
  </Layout>
);

export default About;