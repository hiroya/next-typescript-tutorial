const dev: boolean = process.env.NODE_ENV !== 'production',
      port: number = dev ? 3000 : 8080,
      app          = require('next')({dev}),
      handle       = app.getRequestHandler();

app.prepare()
  .then(() => {
    const server = require('express')();

    server.get("/p/:id", (req, res) => {
      const actualPage = "/post";
      const queryparams = { id: req.params.id };
      app.render(req, res, actualPage, queryparams)
    })

    server.get('*', (req, res) => handle(req, res));

    server.listen(port, (err) => {
        if (err) throw err;
        console.log(`> Ready on http://localhost:${port}`);
    })
  })
  .catch(ex => {
    console.log(ex.stack);
    process.exit(1);
  });

