import * as React from "react";
import Layout from "../components/MyLayout";
import fetch from "isomorphic-unfetch";
import { NextFunctionComponent, NextContext } from "next";

type RequestQuery = { id: string }

type Props = { show?: {[key: string]: any | undefined | null } }

const Post: NextFunctionComponent<Props> = (props) => (
  <Layout>
    <h1>{props.show.name}</h1>
    <p>{props.show.summary.replace(/<[/]?p>/g, '')}</p>
    <img src={props.show.image.medium} />
  </Layout>
);

Post.getInitialProps = async ({ query }: NextContext<RequestQuery>) => {
  const { id } = query;
  const res = await fetch(`https://api.tvmaze.com/shows/${id}`);
  const show = await res.json();
  // client side only
  console.log(`Fetched show: ${show.name}`);

  return { show }
}

export default Post;