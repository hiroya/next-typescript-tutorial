import * as React  from "react";
import Head from "next/head";
import Header from "./Header";

const layoutStyle: { [key: string]: string | number } = {
  margin: 20,
  padding: 20,
  border: "1px solid #DDD"
}

const Layout: React.FunctionComponent = (props) => (
  <div>
    <Head>
      <title>Next Typescript</title>
      <meta charSet="utf-8" />
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    </Head>
    <div style={layoutStyle}>
      <Header />
      {props.children}
    </div>
  </div>
);

export default Layout;